#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
/*
�������� 1. �������� �������� � ������������� ���������.
1) ��������� �������� � �� ������ ����� ���� int;
2) ��������� ����� �, � � ����� m, ����� �����������;
3) ��������� � �������� ������ ����� �.
4) ������� �� ����� �������� ����� � ����� ��������;
5) ���� ���� ���������� �, ���� �������� �������� � = *�?
6) ������ �������� ��������� � �� 7;
7) ���� ���� ���������� �?
8) ���� ���� ���������� �, ���� �������� �������� *�+=5?
*/
int main() {
	int* p;
	int x = 10, y = 20, m[100] = {0};
	p = &y;
    printf("y: %d\n", *p);
    x = *p;
    printf("x = *p: %d\n", x);
    y += 7;
    printf("p after y+=7: %p\n", p);
    *p += 5;
    printf("*p+=5: %d\n", y);
	return 0;
}
