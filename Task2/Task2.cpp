#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
/*
�������� 2. ���� �����. ������� �������� �� ���������:
1) ��������� ����� ������ � ������.
2) ��������� ������� �������� ������.
3) ������� �� ����� ������ ������� � ���������� �������� ������.
4) �������� ������������� ������ � ���������� �������.
*/
int main() {
	int n;
	printf("Enter array size: ");
	scanf("%d", &n);
	int* pm = (int*)malloc(n * sizeof(int));
	for (int i = 0; i < n; i++) {
		*(pm+i) = rand();
		printf("%d ", *(pm + i));
	}
	printf("\nArray size in bytes: %d", n * sizeof(int));
	printf("\nArray element count: %d", n);
	printf("\nFirst and last element addresses: %p, %p\n", pm,pm+n);
	for (int i = 0; i < n / 2; i++) {
		int t = *(pm + (n - i - 1));
		*(pm + (n - i - 1)) = *(pm + i);
		*(pm + i) = t;	
	}
	for (int i = 0; i < n; i++) 
		printf("%d ", *(pm + i));

	free(pm);
	return 0;
}
