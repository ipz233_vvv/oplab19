#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
/*
�������� 4.
�������� ����������� �����. �������� ������ �������� �
������� � ��������� ���������
*/
int main() {
	int n;
	printf("Enter array size: ");
	scanf("%d", &n);
	int* pm = (int*)malloc(n * sizeof(int));
	for (int i = 0; i < n; i++) {
		*(pm + i) = rand() % 11 - 5;
		printf("%d ", *(pm + i));
	}
	printf("\n");
	for (int i = 0; i < n/2*2 ; i+=2) {
		int t = *(pm + i);
		*(pm + i) = *(pm + i + 1);
		*(pm + i + 1) = t;
	}
	for (int i = 0; i < n; i++) {
		printf("%d ", *(pm + i));
	}
	free(pm);
	return 0;
}