#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
/*
�������� 3.
1. �������� �� �������� �� ��������� 0.
2. ������ ���� ������� ������� �������� ������ ������� ��
��������� mas[�-1]+2.
*/
int main() {
	int n;
	printf("Enter array size: ");
	scanf("%d", &n);
	int* pm = (int*)malloc(n * sizeof(int));
	int offset = 0;
	for (int i = 0; i < n; i++) {
		*(pm + i) = rand() % 11 - 5;
		printf("%d ", *(pm + i));
	}
	printf("\n");
	for (int i = 0; i < n; i++) {
		*(pm + i - offset) = *(pm + i);
		offset += *(pm + i) == 0 ? 1 : 0;
	}
	n -= offset;
	pm = (int*) realloc(pm, n*sizeof(int));
	for (int i = 0; i < n; i++) {
		if (*(pm+i) % 2 == 0) {
			n++;
			pm = (int*) realloc(pm, n * sizeof(int));
			for (int j = n - 1; j > i; j--) {
				*(pm + j) = *(pm + j - 1);
			}
			*(pm +i + 1) = *(pm+i) + 2;
			break;
		}
	}
	for (int i = 0; i < n; i++) {
		printf("%d ", *(pm + i));
	}
	free(pm);
	return 0;
}
